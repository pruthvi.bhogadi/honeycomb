Feature: Operations on Favorite API 

Scenario Outline: do Favorite 
	Given make a request with post method with "<entityid>" and "<devicetype>" and "<source>" and "<hmacid>" 
	Then verify the response code should be "<responsecode>" 
	And verify the favouriteid 
	Then launch TicketMaster webApplication
	
	
	Examples:
	
		| entityid | devicetype | source | hmacid | responsecode |
		| K8vZ9171Jo7  | DESKTOP | TMAPP  | cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7 | 200 |
		
Scenario: list all the favorite from my favorite page 
	Given make an GET Api Request 
	Then List all the events